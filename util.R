make_circular_index <- function(left, right, index) {
  return(((index - left) %% (right - left + 1)) + left)
}

comp_expected_value <- function(func, tests_num) {
  result <- 0
  for (i in 1:tests_num) {
    result <- result + (func() / tests_num)
  }
  return(result)
}