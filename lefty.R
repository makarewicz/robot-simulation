source("util.R")

lefty <- function(p, q, l) {
  board <- 1:10
  cur <- 1
  counter <- 0
  while (sum(board) > 0) {
    if (runif(1) < l) {
      if (runif(1) < p) {
        cur <- make_circular_index(1, 10, cur - 1)
      }
    } else {
      if (runif(1) < q) {
        board[cur] <- 0
      }
    }
    counter <- counter + 1  
  }
  return(counter)
}
